//
//  NibLoadableView.swift
//  AudioBook
//
//  Created by Chi Chiem on 30/07/2021.
//

import UIKit

open class NibLoadableView: UIView {
    
    var contentView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        xibSetup()
    }
    
    open func xibSetup() {
        contentView = loadViewFromNib()
        self.addSubview(contentView)
        
        contentView.scaleEqualSuperView()
    }
}
