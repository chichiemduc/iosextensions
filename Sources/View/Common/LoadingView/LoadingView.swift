//
//  LoadingView.swift
//  AudioBook
//
//  Created by Chi Chiem on 02/08/2021.
//

import UIKit

open class LoadingView: UIView {

    open override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.black.withAlphaComponent(0.3)
    }
}
