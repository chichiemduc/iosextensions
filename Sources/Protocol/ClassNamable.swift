//
//  File.swift
//  
//
//  Created by Chi Chiem on 05/08/2021.
//

import Foundation

public protocol ClassNamble {
    var className: String { get }
    static var className: String { get }
}

public extension ClassNamble {
    var className: String {
        return String(describing: type(of: self))
    }

    static var className: String {
        return String(describing: self)
    }
}


