//
//  LoadingShowable.swift
//  AudioBook
//
//  Created by Chi Chiem on 02/08/2021.
//

import UIKit

public protocol LoadingShowable {
    
}

public extension LoadingShowable where Self: UIViewController {
    func showLoading() {
        let loadingView = UINib.nibView(LoadingView.self)
        view.addSubview(loadingView)
        loadingView.scaleEqualSuperView()
    }
    
    func hideLoading() {
        view.subviews.forEach { subview in
            if subview is LoadingView {
                subview.removeFromSuperview()
            }
        }
    }
}
