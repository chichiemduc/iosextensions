//
//  Collection+Extension.swift
//  
//
//  Created by Chi Chiem on 11/05/2021.
//

import Foundation

public extension Collection where Element == Optional<Any> {
    
    func allNil() -> Bool {
        return allSatisfy { $0 == nil }
    }

    func anyNil() -> Bool {
        return first { $0 == nil } != nil
    }

    func allNotNil() -> Bool {
        return !allNil()
    }
}

public extension Collection where Element == String {
    
    func allNotEmpty() -> Bool {
        return allSatisfy({ !$0.isEmpty })
    }
    
    func allEmpty() -> Bool {
        return allSatisfy({ $0.isEmpty })
    }
    
}
