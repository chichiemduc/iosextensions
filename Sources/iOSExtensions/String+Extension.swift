//
//  File.swift
//  
//
//  Created by Chi Chiem on 11/05/2021.
//

import Foundation

public extension CustomStringConvertible {
    func toString() -> String {
        return String(describing: self)
    }
}

public extension String {
    var htmlToAttributedString: NSMutableAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSMutableAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html,
                                                                .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }
    
    var range: NSRange {
        return NSRange(location: 0, length: self.count)
    }
}
