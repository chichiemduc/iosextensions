//
//  Array+Extension.swift
//  LYFE
//
//  Created by Chi Chiem on 6/9/20.
//  Copyright © 2020 Defide inc. All rights reserved.
//

import Foundation

public extension Array where Element: Equatable {
    mutating func remove(_ element: Element) {
        guard let index = self.firstIndex(of: element) else { return }
        self.remove(at: index)
    }
}

public extension Array {
    func get(_ index: Int) -> Element? {
        guard index < count else { return nil }
        return self[index]
    }
}
