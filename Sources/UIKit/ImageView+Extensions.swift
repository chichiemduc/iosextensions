//
//  ImageView+Extensions.swift
//  AudioBook
//
//  Created by Chi Chiem on 28/07/2021.
//

import UIKit
import Kingfisher

public extension UIImageView {
    func loadImage(_ urlString: String?,
                   placeholder: UIImage? = UIImage(named: "placeholder-portrait"),
                   indicatorType: IndicatorType = .none) {
        self.image = placeholder
        guard let urlString = urlString else {
            return
        }
        let url = URL(string: urlString)
        let processor = DownsamplingImageProcessor(size: bounds.size)
        kf.indicatorType = indicatorType
        
        kf.setImage(
            with: url,
            placeholder: placeholder,
            options: [.processor(processor),
                      .scaleFactor(UIScreen.main.scale),
                      .transition(.none),
                      .cacheOriginalImage],
            completionHandler: { result in
                switch result {
                case .success:
                    break
                case .failure(let error):
                    print("Kingfisher load failed: \(error.localizedDescription)")
                }
            }
        )
    }
}
