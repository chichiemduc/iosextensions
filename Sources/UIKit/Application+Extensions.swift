//
//  Application+Extensions.swift
//  AudioBook
//
//  Created by Chi Chiem on 28/07/2021.
//

import UIKit

public extension UIApplication {
    static var keyWindow: UIWindow? {
        UIApplication.shared.windows.first(where: { $0.isKeyWindow })
    }
    
    static var windowInterfaceOrientation: UIInterfaceOrientation? {
        if #available(iOS 13.0, *) {
            return UIApplication.keyWindow?.windowScene?.interfaceOrientation
        } else {
            return UIApplication.shared.statusBarOrientation
        }
    }
}
