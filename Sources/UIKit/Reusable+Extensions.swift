//
//  Reusable+Extensions.swift
//  AudioBook
//
//  Created by Chi Chiem on 28/07/2021.
//

import UIKit

public extension UITableView {
    // Register with nib
    func registerNib<T: UITableViewCell>(type: T.Type) {
        register(UINib(nibName: type.className, bundle: nil), forCellReuseIdentifier: type.className)
    }
    
    // Register with class
    func registerClass<T: UITableViewCell>(type: T.Type) {
        register(type, forCellReuseIdentifier: type.className)
    }
    
    func dequeueReusableCell<T: UITableViewCell>(type: T.Type) -> T {
        return self.dequeueReusableCell(withIdentifier: type.className) as! T
    }
}

public extension UICollectionView {
    // Register with nib
    func registerNib<T: UICollectionViewCell>(type: T.Type) {
        register(UINib(nibName: type.className, bundle: nil), forCellWithReuseIdentifier: type.className)
    }
    
    // Register with class
    func registerClass<T: UICollectionViewCell>(type: T.Type) {
        register(type, forCellWithReuseIdentifier: type.className)
    }
    
    func dequeueReusableCell<T: UICollectionViewCell>(type: T.Type, for indexPath: IndexPath) -> T {
        return self.dequeueReusableCell(withReuseIdentifier: type.className, for: indexPath) as! T
    }
}
