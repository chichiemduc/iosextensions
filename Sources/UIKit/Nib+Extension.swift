//
//  Nib+Extension.swift
//  AudioBook
//
//  Created by Chi Chiem on 30/07/2021.
//

import UIKit

public extension UINib {
    static func instantiate<T: UIView>(_ type: T.Type) -> UINib {
        return UINib.init(nibName: type.className, bundle: .main)
    }
    
    static func nibView<T: UIView>(_ type: T.Type) -> T {
        let nib = UINib.instantiate(type)
        return nib.instantiate(withOwner: type, options: nil).first as! T
    }
    
}
