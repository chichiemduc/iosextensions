import XCTest

import iOSExtensionsTests

var tests = [XCTestCaseEntry]()
tests += iOSExtensionsTests.allTests()
XCTMain(tests)
