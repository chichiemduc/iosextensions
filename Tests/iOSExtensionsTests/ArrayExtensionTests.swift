//
//  File.swift
//  
//
//  Created by Chi Chiem on 11/05/2021.
//

import XCTest
import iOSExtensions

class ArrayExtensionTests: XCTestCase {
    private var data: [Int]!
    
    override func setUp() {
        super.setUp()
        data = [1]
    }
    
    func testGetValidElementFromArray() {
        let expected = 1
        var result = 0
        
        result = data.get(0)!
        
        XCTAssertEqual(expected, result)
    }
    
    func testGetInvalidElementFromArray() {
        let expected: Int? = nil
        var result: Int? = nil
        
        result = data.get(9)
        
        XCTAssertEqual(expected, result)
    }
    
    func testRemoveElementFromArray() {
        data.remove(1)
        XCTAssertTrue(data.isEmpty)
    }
}


