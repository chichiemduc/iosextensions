// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "iOSExtensions",
    platforms: [.iOS(.v13)],
    products: [
        .library(
            name: "iOSExtensions",
            targets: ["iOSExtensions"]),
    ],
    dependencies: [
        .package(url: "https://github.com/onevcat/Kingfisher", from: "7.0.0")
    ],
    targets: [
        .target(
            name: "iOSExtensions",
            dependencies: ["Kingfisher"],
            path: "Sources")
    ]
)
